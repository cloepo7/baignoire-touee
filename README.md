# Baignoire trouée
Ce projet a pour but de montrer notre évolution dans la Programmation Avancée Orientée Objet, pour le semestre 2 de la première année de Master MIAGE. Il présente le remplissage d'une baignoire suivant le débit d'entrée et un certain nombre de trou dans la coque.

## Table des matières

  * Installation
  * Interface
  * Traitement
  * Références & Librairies


Ce programme a été écrit par Cloé POIROT (M1 MIAGE, mai 2022).
