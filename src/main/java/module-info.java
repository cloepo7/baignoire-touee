module com.example.baignoiretrouee_poirot {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;


    opens com.example.baignoiretrouee_poirot to javafx.fxml;
    exports com.example.baignoiretrouee_poirot;
}