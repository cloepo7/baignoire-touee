package com.example.baignoiretrouee_poirot;

public class Trou {
    int debit;

    public Trou(int debit) {
        this.debit = debit;
    }

    public int getDebit() {
        return debit;
    }

    public void setDebit(int debit) {
        this.debit = debit;
    }

    @Override
    public String toString() {
        return "Trou avec un débit de " + debit + " litre(s) par minute.";
    }
}
