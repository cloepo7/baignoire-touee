package com.example.baignoiretrouee_poirot;

import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.util.Duration;

public class VidageService extends ScheduledService<Integer> {
        private Baignoire baignoire;

    public VidageService(Baignoire baignoire) {
        super();
        this.baignoire = baignoire;
        this.setPeriod(Duration.seconds(1));
    }

    /**
     * création task vidage de la baignoire (pour thread)
     * @return task
     */
    @Override
    public Task<Integer> createTask() {
        return new Task<>() {
            @Override
            protected Integer call() {
                return baignoire.vidage();
            }
        };
    }
}
