package com.example.baignoiretrouee_poirot;

import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.util.Duration;

public class RemplissageService extends ScheduledService<Integer> {
    private Baignoire baignoire;

    public RemplissageService(Baignoire baignoire) {
        super();
        this.baignoire = baignoire;
        this.setPeriod(Duration.seconds(1));
    }

    /**
     * création task remplissage de la baignoire (pour thread)
     * @return task
     */
    @Override
    public Task<Integer> createTask(){
        return new Task<>() {
            @Override
            protected Integer call() {
                return baignoire.remplissage();
            }
        };
    }
}
