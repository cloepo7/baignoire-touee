package com.example.baignoiretrouee_poirot;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.ImageView;

import javafx.concurrent.WorkerStateEvent;

import java.util.ArrayList;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;


public class HelloController {
    //definition objets
    private static final Logger LOG = Logger.getLogger(HelloController.class.getName());
    private ArrayList<Trou> listeTrou = new ArrayList<Trou>();
    Alert alert = new Alert(AlertType.INFORMATION);

    //definition objets interface
    @FXML
    private Label trouText;
    @FXML
    private Label debitRobinet;
    @FXML
    private Label volume;
    @FXML
    private Label pourcentage;
    @FXML
    private Slider sliderRobinet;
    @FXML
    private Slider sliderVolume;
    @FXML
    private Slider sliderTrou;
    @FXML
    private ImageView imageBaignoire;


    @FXML
    /**
     * Affichage du débit du robinet au mouvement du slider
     */
    protected void onDragRobinet() { debitRobinet.setText( (int) sliderRobinet.getValue() + " L/sec"); }


    @FXML
    /**
     * Affichage du débit du robinet au mouvement du slider
     */
    protected void onDragVolume() { volume.setText( (int) sliderVolume.getValue() + " L"); }


//------------------------------------------------- Gestion Trous ---------------------------------------------------------------

    @FXML
    /**
     * Ajout de trou
     * Ajoute un trou à la liste de trous, en prenant le débit mis sur le slider, dans le code
     * et affiche celui ci avec son débit dans l'interface
     */
    protected void onHelloButtonClick() {
        Trou trou = new Trou((int) sliderTrou.getValue());
        listeTrou.add(trou);
        trouText.setText(trouText.getText() + "\nUn trou de " + (int) sliderTrou.getValue());
    }

    @FXML
    /**
     * Fonction de suppression des trous
     * Vide la liste de trous dans le code et affiche un texte vide sur l'interface
     */
    protected void onReparerButton() {
        listeTrou.clear();
        trouText.setText("");
    }

//------------------------------------------------- Traitement ---------------------------------------------------------------

    @FXML
    /**
     * Traitement
     * Remise à zéro du traitement (si il a déjà été lancé) puis lancement des threads pour le remplissage et le vidage
     * simultané de la baignoire. Affichage du pourcentage de remplissage et animation de la baignoire (image).
     */
    protected void onRemplir() {
        debitRobinet.setText( (int) sliderRobinet.getValue() + " L/sec");
        volume.setText( (int) sliderVolume.getValue() + " L");
        //(re)mise à zéro
        Baignoire baignoire = new Baignoire();
        baignoire.setListeTrous(listeTrou);
        RemplissageService remplissageService = new RemplissageService(baignoire);
        VidageService vidageService = new VidageService(baignoire);

        baignoire.setVolume((int) sliderVolume.getValue());
        baignoire.setNiveauRemplissage();
        baignoire.setDebitRobinet((int) sliderRobinet.getValue());

        //configuration du thread de remplissage
        remplissageService.setOnSucceeded((WorkerStateEvent event) -> {
            pourcentage.setText(baignoire.getNiveauRemplissage() + "%");
            //evolution de l'image dans l'interface
            if(baignoire.setImage()){
                imageBaignoire.setImage(baignoire.getImage());
            }
            //arret des threads lorsque le niveau max est atteint
            if(baignoire.getNiveauRemplissage() == 100){
                //affichage du bilan via une alerte
                alert.setTitle("Bilan");
                alert.setHeaderText("Quantité d'eau totale : " + baignoire.getQttTotale() + " L"
                                    + "\n Temps nécessaire : " + baignoire.getTimer() + " secondes");
                alert.show();
                //arret des threads
                remplissageService.cancel();
                vidageService.cancel();
            }
        });

        //configuration du thread de vidage (trous)
        vidageService.setOnSucceeded((WorkerStateEvent event) -> {
            //arret si le bain est plein
            if(baignoire.getNiveauRemplissage() == 100){
                vidageService.cancel();
            }
        });

        //lancement des threads
        vidageService.start();
        remplissageService.start();
    }



}