package com.example.baignoiretrouee_poirot;

import javafx.scene.image.Image;

import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Logger;
import javafx.scene.image.Image;

public class Baignoire {
    private static final Logger LOG = Logger.getLogger(HelloController.class.getName());

    int debitRobinet;
    int niveauRemplissage;
    int qttEau;
    int timer;
    int qttTotale;
    int volume;
    ArrayList<Trou> listeTrous = new ArrayList<Trou>();
    Image image = new Image(String.valueOf(getClass().getResource("images_baignoires/baignoire00.png")));

    public Baignoire() {
        this.debitRobinet = 0;
        this.niveauRemplissage=0;
        this.qttEau = 0;
        this.timer=0;
        this.qttTotale=0;
        this.listeTrous = null;
    }

    public int getDebitRobinet() {
        return debitRobinet;
    }

    public void setDebitRobinet(int debitRobinet) {
        this.debitRobinet = debitRobinet;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getQttEau() {return qttEau;}

    public void setQttEau(int qttEau) {
        this.qttEau = qttEau;
        this.setNiveauRemplissage();
    }

    public int getQttTotale() {
        return qttTotale;
    }

    public void setQttTotale(int qttEau) {
        this.qttTotale += qttEau;
    }

    public int getTimer() {
        return timer;
    }

    public void setTimer(int timer) {
        this.timer = timer;
    }

    public ArrayList<Trou> getListeTrous() {
        return listeTrous;
    }

    public void setListeTrous(ArrayList<Trou> listeTrous) {
        this.listeTrous = listeTrous;
    }

    public int getNiveauRemplissage() {
        return niveauRemplissage;
    }

    public void setNiveauRemplissage() {
        this.niveauRemplissage = (int) (((float) this.qttEau/this.volume) * 100);
    }

    public Image getImage() {
        return image;
    }

    /**
     * Fonction pour l'animation de la baignoire
     * Récupèration de l'image suivant le niveau de l'eau présent dans la baignoire
    */
    public boolean setImage() {
        int niveau = niveauRemplissage/10;
        System.out.println(niveauRemplissage);
        if(niveau%2 == 0){
            try {
                String name = "images_baignoires/baignoire" + niveau + "0.png";
                URL url = getClass().getResource(name);
                this.image = new Image(String.valueOf((url)));
                return true;
            }
            catch (Exception e) {
                LOG.severe("Image non trouvée");
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Baignoire dont le robinet a un debit de " + getDebitRobinet() +
                ", et possédant " + getListeTrous().size() + " trou(s).";
    }


//--------------------------------------Fonctions--------------------------------------------

    /**
     * Fonction de remplissage de la baignoire
     * S'organise via le débit du robinet de la baignoire, toutes les 1 secondes
     */
    public int remplissage(){
        try {
            Thread.sleep(1000);
            }
        catch(InterruptedException e){
            LOG.warning(e.getMessage());
        }
        //incrementation du timer à chaque execution
        this.setTimer(this.getTimer()+1);
        //mise à jour du niveau de l'eau
        System.out.println("Volume : " + getVolume() + "  |  qttEau : " + getQttEau() + "  |  qttTotale : " + getQttTotale());
        if(qttEau+debitRobinet <= volume) {
            this.setQttEau(qttEau+debitRobinet);
            this.setQttTotale(debitRobinet);
            return this.niveauRemplissage;
        }
        niveauRemplissage=100;
        return niveauRemplissage;
    }

    /**
    * Fonction de vidage de la baignoire
    * S'organise via la liste des trous de la baignoire, toutes les 1 secondes
    */
    public int vidage(){
        try {
            Thread.sleep(1000);
        }
        catch(InterruptedException e){
            LOG.warning(e.getMessage());
        }
        //perte suivant les débits des trous, ne passe jamais sous 0%
        if(this.qttEau-calculPerte() <= 0){
            niveauRemplissage=0;
            return niveauRemplissage;
        }
        setQttEau(qttEau-calculPerte());
        return (this.niveauRemplissage);
    }

    /**
    * Fonction de calcul de perte
     * Défini la perte d'eau dans la baignoire en fonction des trous et de leurs débits respectifs
    */
    public int calculPerte(){
        int perte=0;
        if(!listeTrous.isEmpty()){
            for(Trou trou : listeTrous){
                perte=perte+trou.getDebit();
            }
        }
        return perte;
    }


}
