# Références & Librairies

Ce projet est basé sur les cours et les TD effectués durant le 2e semestre de Master 1 MIAGE (2022). Le langage utilisé est le [JAVA](https://www.oracle.com/java/technologies/) (version 11.0.13).

[Maven](https://maven.apache.org/) permet à cette application une installation et un import de librairies plus aisé pour l'utilisateur.

Enfin [JavaFX ](https://openjfx.io/) (version 18.0.1) a été très utile afin de créer l'interface graphique et faciliter son développement et l'utilisation de ses objets.
