# Traitement

* Initialisation
* Threads
* Bilan

Le traitement de cette application se fait grâce à des threads : l'un remplit la baignoire et l'autre le vide. Chacun d'entre eux dépend des paramètres de la baignoire entrés par l'utilisateur dans l'interface.

## Initialisation

Plusieurs objets ont été créés afin de permettre faire cette application : des tasks ont été nécessaires pour l'execution des méthodes en threads pour le remplissage et le vidage de la baignoire, un objet Baignoire permettant la manipulation de tous les paramètres utiles aux calculs et à l'affichage de l'application et enfin des objets Trous, afin de compléter les attributs qui composent notre baignoire.

Le lancement de ce programme amène tout d'abord à l'initialisation de ces objets afin d'avoir un système initial correct. Initialiser dans la fonction, et non dans les objets de la classe, permet de les ré-initialiser lorsque l'utilisateur execute de nouveau le programme : les objets sont à leur état initial et prêts à fonctionner sans problème d'initialisation.

## Threads

Tout d'abord, la remplissage de la baignoire se fait par un premier thread effectuant l'ajout d'une certaine quantité d'eau dans la baignoire à condition que celle-ci ne soit pas déjà totalement rempli. Dans ce cas, le thread s'arrête car le programme à atteint son but. La quantité ajoutée dépend du débit du robinet. Le niveau de remplissage est obtenu en calculant le rapport entre le volume de la baignoire et la quantité d'eau actuellement présente. Si l'ajout de l'eau dépasse les 100%, le niveau s'arrête à 100% ce qui entraine l'arrêt du thread.

D'un autre côté, un second thread afin d'effectuer le vidage de la baignoire. La quantité totale d'eau perdue chaque seconde est la somme des débits de tous les trous ajoutés par l'utilisateur.

Si les paramètres rentrés par l'utilisateur font que la baignoire ne peut se remplir, les threads ne s'arrêteront pas après un temps donnés. En effet, je suis partie du principe que celui-ci peut relancer une execution à tout moment en changeant ses paramètres et peut facilement voir que la baignoire ne se remplit pas, il n'a donc pas besoin d'avoir un timer qui lancerait une alerte et qui stopperait l'execution du programme. De plus cette solution aurait d'autant plus ralentit l'execution du programme et aurait pu arrêter le remplissage alors que les conditions sont favorables mais très faibles.

## Bilan

Un bilan du remplissage s'affiche à l'arrêt des threads en montrant la quantité d'eau qui a été nécessaire ainsi que le temps associé. Ces deux paramètres sont initialisés à chaque nouveau lancement des threads et sont calculés lors du remplissage de la baignoire.

La solution des timers n'a pas été retenue due au fait qu'ils pourraient ralentir le traitement. De plus, les threads fonctionnent chaque seconde : il est alors facile de modifier le code afin d'obtenir un remplissage à la minute par exemple.
