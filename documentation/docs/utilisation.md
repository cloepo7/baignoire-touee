# Utilisation

* Entrées utilisateur 👀️
* Lancement 🚀️
* Animation 🎉️

L'interface utilisateur de cette application est créée grâce à JavaFX (version 18.0.1). Il présente toutes les actions faisables par l'utilisateur ainsi que la représentation des résultats en cours d'execution.

![image.png](./assets/1651590769939-image.png)

## Entrées utilisateur 👀️

On peut remarquer la présence de trois sliders sur cette  interface. Le premier, en bas à gauche, permet à l'utilisateur d'ajouter des trous avec des tailles différentes, comprises entre 1 et 5, mais il est également possible de les supprimer. De même, le deuxième slider aide à la sélection d'un valeur pour le débit d'ajout d'eau dans la baignoire.

Les sliders ont l'avantage de laisser moins de liberté à l'utilisateur dans son entrée de valeur : une espace de texte accepterait l'entrée de valeurs alphanumériques, quand cette application ne traite que des numériques. De plus, les valeurs entrées pourraient dépasser les limites que nous souhaitons. Il faudrait ainsi gérer les erreurs d'entrées dans le code. Les sliders n'accordent que l'entrée de numériques délimités en amont. Moins un utilisateurs a de liberté dans ses actions, plus le nombre d'exceptions rencontrables est faible.

Le bouton "Réparer" supprime tous les trous créés précédemment. Il aurait été possible de les supprimer un à un mais cette option implique une action répétitive si beaucoup de trous ont été générés. C'est pourquoi j'ai choisi de supprimer la totalité.

## Lancement 🚀️

Le lancement du traitement de cete application se fait avec le bouton "remplir" sur l'interface. Les trous et le débit de remplissage sont pris en compte et les threads sont lancés afin de remplir et vider la baignoire en simultané.

Il est possible de relancer l'execution lors d'un traitement non terminé : il suffit de cliquer de nouveau sur "remplir" avec les nouveaux paramètres. De même, il est important de noter que, lors d'un traitement, il est possible de colmater les trous. Cela a pour effet de supprimer les trous pendant le traitement et ainsi de ne plus avoir de perte sur cette execution.

## Animation 🎉️

L'interface utilisateur permet de suivre l'avancement du remplissage de notre baignoire. En effet, deux éléments le permettent : le pourcentage de remplissage et l'image de la baignoire. Chacun de ces éléments suit le niveau de remplissage de la baignoire en temps réel afin de montrer sa progression.

Néanmoins, l'image de la baignoire évolue en fonction du niveau de remplissage : à chaque vingtaine, celle-ci change pour créer une animation de remplissage. Cet effet de bon peut s'expliquer par un système de led très couteux sur notre baignoire. On ne peut donc se permettre d'afficher le remplissage exact, c’est pour cela que l'affichage est saccadé.

![image.png](./assets/1651589368184-image.png)

Un bilan à la fin de l'execution s'affiche afin de résumer les résultats : une fenetre en pop-up apparait alors pour mettre en avant la quantité d'eau qui vient d'être utilisée ainsi que le temps d'execution.
